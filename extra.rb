# frozen_string_literal: true

# Class for vehicles extras
class Extra
  attr_accessor :price, :name

  def initialize(name, price)
    @price = price
    @name = name
  end
end
