# frozen_string_literal: true

require_relative 'extra'

# Parent class for vehicles
class Vehicle
  attr_reader :id, :wheels, :color, :brand, :price, :extras
  def initialize(wheels, color, brand, price)
    @id = SecureRandom.uuid
    @wheels = wheels
    @color = color
    @brand = brand
    @price = price
    @extras = []
  end

  def add_extra(extra)
    @extras << extra if extra.is_a? Extra
  end

  class << self
    def build_vehicles(wheels)
      vehicles = []
      5.times do
        color = Faker::Color.color_name
        brand = Faker::Vehicle.make
        price = rand(5000..300_00)
        vehicles << new(wheels, color, brand, price)
      end
      vehicles
    end
  end
end

# Vehicle subclass for Cars
class Car < Vehicle
  def initialize(wheels, color, brand, price)
    super wheels, color, brand, price
  end

  class << self
    def build_vehicles
      super(4)
    end
  end
end

# Vehicle subclass for Trucks
class Truck < Vehicle
  def initialize(wheels, color, brand, price)
    super wheels, color, brand, price
  end

  class << self
    def build_vehicles
      super(2 * rand(2..8))
    end
  end
end
