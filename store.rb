# frozen_string_literal: true

require_relative 'vehicle'

# Class for Store, here will be stored all vehicles and showed to the user
class Store
  attr_reader :vehicles
  def initialize
    @vehicles = []
  end

  def add(vehicles)
    if vehicles.is_a? Array
      vehicles.each { |v| @vehicles << v if v.is_a? Vehicle }
    elsif vehicles.is_a? Vehicle
      @vehicles << vehicles
    end
    @vehicles
  end

  def remove_vehicle(id)
    @vehicles.reject! { |v| v.id == id }
  end

  def get_quote(id)
    vehicle = @vehicles.detect { |v| v.id == id }
    total = 0
    puts 'Detail:'
    puts "\tQuote: #{vehicle.id}"
    puts "\tFeatures"
    puts "\tType: #{vehicle.class}"
    puts "\tColor: #{vehicle.color}"
    puts "\tNumber of Wheels: #{vehicle.wheels}"
    puts "\tBrand: #{vehicle.brand}"
    puts "\tPrice: $#{vehicle.price}"
    puts '-' * 75
    total += vehicle.price
    if vehicle.extras
      extras = vehicle.extras
      puts 'Extras:'
      extras.each do |extra|
        puts "\t#{extra.name}: $#{extra.price}"
        total += extra.price
      end
    end
    puts '-' * 75
    puts "\tTotal: $#{total}"
  end
end
