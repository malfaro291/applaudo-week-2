# frozen_string_literal: true

require 'faker'
require 'securerandom'
require_relative 'store'

cars = Car.build_vehicles
trucks = Truck.build_vehicles

trucks[2].add_extra(Extra.new('Radio', 55))
trucks[2].add_extra(Extra.new('Sunroof', 250))

store = Store.new
store.add(cars)
store.add(trucks)
new_car = Car.new(4, 'red', Faker::Vehicle.make, 5080)
store.add(new_car)
new_car.add_extra(Extra.new('A/C', 195))

puts
puts 'List of vehicles before deletion'
puts

store.vehicles.each { |item| p item }

print "\nCar to delete: "
p store.vehicles[2]
puts

puts
puts 'List of vehicles after deletion'
puts

store.remove_vehicle(store.vehicles[2].id)
store.vehicles.each { |item| p item }
puts
store.get_quote(store.vehicles[6].id)
