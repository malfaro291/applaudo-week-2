create database OnlineStore;

create type check_status as enum('cancelled', 'delivered','in transit', 'payment due', 'pick up available', 'problem', 'processing', 'returned');

create table customer (
  id serial,
  first_name varchar(75),
  last_name varchar(75),
  address text,
  phone varchar(15)
);

create table customer_order (
  id serial,
  order_date date,
  customer_id integer,
  total decimal,
  status check_status
);

create table product (
  id serial,
  sku varchar(250),
  name varchar(150),
  description text,
  price decimal,
  stock integer
);

create table order_line (
  id serial,
  order_id integer,
  product_id integer,
  quantity integer,
  price decimal,
  total decimal
);

alter table customer add constraint PK_Customer PRIMARY KEY (id);
alter table customer_order add constraint PK_Order PRIMARY KEY (id);
alter table product add constraint PK_Product PRIMARY KEY (id);
alter table order_line add constraint PK_OrderLine PRIMARY KEY (id);

alter table customer_order add constraint FK_Order_Customer FOREIGN KEY (customer_id) REFERENCES customer(id);
alter table order_line add constraint FK_OrderLine_Product FOREIGN KEY (product_id) REFERENCES product(id);
alter table order_line add constraint FK_OrderLine_Order FOREIGN KEY (order_id) REFERENCES customer_order(id) ON DELETE CASCADE;

-- Inserts
INSERT INTO customer (first_name, last_name, address, phone) VALUES
      ('Manuel', 'Alfaro', '545 Rockwell Rd. Carlisle, PA 17013', '907-683-4462'),
      ('Gabriela', 'Flores', '9212 Creek St. Williamsport, PA 17701', '773-920-8953'),
      ('Jorge', 'Rodriguez', '32 Hanover Street Patchogue, NY 11772', '406-652-0424'),
      ('Jose', 'Martinez', '78 Littleton Drive Greensburg, PA 15601', '812-414-3830'),
      ('Fernando', 'Lara', '13 E. Branch St. Pompano Beach, FL 33060', '831-607-8849');

INSERT INTO product (sku, name, description, price, stock) VALUES
      ('416855601', 'DIP Switch', 'DIP Switch', 54.94, 52),
      ('988558635', 'Toggle Switch', 'Toggle Switch', 7.7, 23),
      ('436444719', 'Slide Switch', 'Slide Switch', 8.81, 62),
      ('170750924', 'Fuse', 'Fuse',44.99, 36),
      ('246698236', 'Circuit Breaker', 'Circuit Breaker', 2.42, 8),
      ('065858667', 'Toggle Switch', 'Toggle Switch', 32.74, 43),
      ('829114529', 'Piezoelectric device', 'Piezoelectric device', 82.89, 8),
      ('296093492', 'Footswitch,', 'Footswitch,',64.72, 48),
      ('320839069', 'Power Cord', 'Power Cord', 63.24, 41),
      ('016003144', 'Ultrasonic Motor', 'Ultrasonic Motor', 62.4, 99),
      ('601811409', 'Piezoelectric device', 'Piezoelectric device', 11.52, 14),
      ('426907966', 'Power Cord', 'Power Cord', 72.73, 31),
      ('195558973', 'Slide Switch', 'Slide Switch', 85.55, 65),
      ('242682415', 'Footswitch,', 'Footswitch,',45.04, 62),
      ('221524727', 'Power Cord', 'Power Cord', 26.28, 18),
      ('307534682', 'Toggle Switch', 'Toggle Switch', 38.88, 10),
      ('312966895', 'DIP Switch', 'DIP Switch', 63.79, 67),
      ('520891795', 'Fuse', 'Fuse',19.87, 61),
      ('711209040', 'Piezoelectric device', 'Piezoelectric device', 70.03, 93),
      ('912991824', 'Slide Switch', 'Slide Switch', 44.29, 9);

INSERT INTO customer_order (order_date, customer_id, total, status) VALUES
      ('2019-07-14', 5, 0, 'processing')            ,
      ('2019-07-28', 1, 0, 'processing'),
      ('2019-07-03', 3, 0, 'processing'),
      ('2019-07-08', 5, 0, 'payment due'),
      ('2019-07-29', 2, 0, 'processing'),
      ('2019-08-04', 4, 0, 'returned'),
      ('2019-07-23', 5, 0, 'processing'),
      ('2019-07-17', 3, 0, 'delivered'),
      ('2019-08-03', 3, 0, 'pick up available'),
      ('2019-07-06', 5, 0, 'returned');

INSERT INTO order_line (order_id, product_id, quantity, price, total) VALUES
(6, 4, 17, (select price from product where id = 4), 0),
(7, 20, 24, (select price from product where id = 20), 0),
(9, 4, 17, (select price from product where id = 4), 0),
(2, 19, 8, (select price from product where id = 19), 0),
(3, 10, 13, (select price from product where id = 10), 0),
(4, 19, 24, (select price from product where id = 19), 0),
(1, 5, 1, (select price from product where id = 5), 0),
(10, 10, 10, (select price from product where id = 10), 0),
(1, 18, 19, (select price from product where id = 18), 0),
(6, 19, 19, (select price from product where id = 19), 0),
(6, 18, 5, (select price from product where id = 18), 0),
(5, 6, 19, (select price from product where id = 6), 0),
(7, 14, 0, (select price from product where id = 14), 0),
(4, 6, 24, (select price from product where id = 6), 0),
(5, 4, 19, (select price from product where id = 4), 0),
(7, 13, 9, (select price from product where id = 13), 0),
(5, 17, 6, (select price from product where id = 17), 0),
(9, 4, 2, (select price from product where id = 4), 0),
(8, 20, 16, (select price from product where id = 20), 0),
(10, 2, 5, (select price from product where id = 2), 0);

update order_line set total = quantity * price;
update customer_order set total = (select sum(total) from order_line where order_id = customer_order.id);

-- Queries
-- 1.
select * from product;
-- 2.
select * from customer;
-- 3.
select * from order_line where order_id = 5;
-- 4.
select co.id, co.order_date, co.total, co.status  from order_line ol right join customer_order co on ol.order_id = co.id where ol.id = 15;
-- 5.
select co.id, co.order_date from product pr right join order_line ol on pr.id = ol.product_id 
                                      right join customer_order co on ol.order_id = co.id where pr.id = 10;
-- 6.
select sum(ol.total) as "Total of Sales" from product pr right join order_line ol on pr.id = ol.product_id where pr.id = 19;
-- 7.
update product set price = price * 1.5 where id = 2;
-- 8.
select cr.id, cr.first_name, cr.last_name from product pr right join order_line ol on pr.id = ol.product_id 
                                                          right join customer_order co on ol.order_id = co.id 
                                                          right join customer cr on co.customer_id = cr.id where pr.id = 10;
-- 9.
select * from customer_order where order_date between '2019-07-25' and '2019-08-05';
-- 10.
select * from product where price > 4.5;
-- 11.
select pr.sku, pr.name from product pr left join order_line ol on pr.id = ol.product_id
                        left join customer_order co on ol.order_id = co.id
                        left join customer cr on co.customer_id = cr.id where cr.id = 3;
-- 12.
select sum(ol.quantity) from product pr left join order_line ol on pr.id = ol.product_id
                        left join customer_order co on ol.order_id = co.id
                        left join customer cr on co.customer_id = cr.id where cr.id = 3 and order_date between '2019-07-01' and '2019-07-20';
-- 13.
select pr.id, pr.name, sum(ol.quantity) from product pr right join order_line ol on pr.id=ol.product_id group by pr.id 
having sum(ol.quantity) = (select sum(quantity) from order_line group by product_id order by sum(quantity) DESC LIMIT 1);
-- 14.
delete from customer_order where id = 6;
