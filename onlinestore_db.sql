--
-- PostgreSQL database dump
--

-- Dumped from database version 10.9 (Ubuntu 10.9-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.9 (Ubuntu 10.9-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: check_status; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.check_status AS ENUM (
    'cancelled',
    'delivered',
    'in transit',
    'payment due',
    'pick up available',
    'problem',
    'processing',
    'returned'
);


ALTER TYPE public.check_status OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: customer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.customer (
    id integer NOT NULL,
    first_name character varying(75),
    last_name character varying(75),
    address text,
    phone character varying(15)
);


ALTER TABLE public.customer OWNER TO postgres;

--
-- Name: customer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.customer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.customer_id_seq OWNER TO postgres;

--
-- Name: customer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.customer_id_seq OWNED BY public.customer.id;


--
-- Name: customer_order; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.customer_order (
    id integer NOT NULL,
    order_date date,
    customer_id integer,
    total numeric,
    status public.check_status
);


ALTER TABLE public.customer_order OWNER TO postgres;

--
-- Name: customer_order_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.customer_order_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.customer_order_id_seq OWNER TO postgres;

--
-- Name: customer_order_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.customer_order_id_seq OWNED BY public.customer_order.id;


--
-- Name: order_line; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.order_line (
    id integer NOT NULL,
    order_id integer,
    product_id integer,
    quantity integer,
    price numeric,
    total numeric
);


ALTER TABLE public.order_line OWNER TO postgres;

--
-- Name: order_line_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.order_line_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.order_line_id_seq OWNER TO postgres;

--
-- Name: order_line_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.order_line_id_seq OWNED BY public.order_line.id;


--
-- Name: product; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.product (
    id integer NOT NULL,
    sku character varying(250),
    name character varying(150),
    description text,
    price numeric,
    stock integer
);


ALTER TABLE public.product OWNER TO postgres;

--
-- Name: product_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.product_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_id_seq OWNER TO postgres;

--
-- Name: product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.product_id_seq OWNED BY public.product.id;


--
-- Name: customer id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customer ALTER COLUMN id SET DEFAULT nextval('public.customer_id_seq'::regclass);


--
-- Name: customer_order id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customer_order ALTER COLUMN id SET DEFAULT nextval('public.customer_order_id_seq'::regclass);


--
-- Name: order_line id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_line ALTER COLUMN id SET DEFAULT nextval('public.order_line_id_seq'::regclass);


--
-- Name: product id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product ALTER COLUMN id SET DEFAULT nextval('public.product_id_seq'::regclass);


--
-- Data for Name: customer; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.customer (id, first_name, last_name, address, phone) FROM stdin;
1	Manuel	Alfaro	545 Rockwell Rd. Carlisle, PA 17013	907-683-4462
2	Gabriela	Flores	9212 Creek St. Williamsport, PA 17701	773-920-8953
3	Jorge	Rodriguez	32 Hanover Street Patchogue, NY 11772	406-652-0424
4	Jose	Martinez	78 Littleton Drive Greensburg, PA 15601	812-414-3830
5	Fernando	Lara	13 E. Branch St. Pompano Beach, FL 33060	831-607-8849
\.


--
-- Data for Name: customer_order; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.customer_order (id, order_date, customer_id, total, status) FROM stdin;
1	2019-07-14	5	379.95	processing
2	2019-07-28	1	560.24	processing
3	2019-07-03	3	811.2	processing
4	2019-07-08	5	2466.48	payment due
5	2019-07-29	2	1859.61	processing
7	2019-07-23	5	1832.91	processing
8	2019-07-17	3	708.64	delivered
9	2019-08-03	3	854.81	pick up available
10	2019-07-06	5	662.5	returned
\.


--
-- Data for Name: order_line; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.order_line (id, order_id, product_id, quantity, price, total) FROM stdin;
2	7	20	24	44.29	1062.96
3	9	4	17	44.99	764.83
4	2	19	8	70.03	560.24
5	3	10	13	62.4	811.2
6	4	19	24	70.03	1680.72
7	1	5	1	2.42	2.42
8	10	10	10	62.4	624.0
9	1	18	19	19.87	377.53
12	5	6	19	32.74	622.06
13	7	14	0	45.04	0.00
14	4	6	24	32.74	785.76
15	5	4	19	44.99	854.81
16	7	13	9	85.55	769.95
17	5	17	6	63.79	382.74
18	9	4	2	44.99	89.98
19	8	20	16	44.29	708.64
20	10	2	5	7.7	38.5
\.


--
-- Data for Name: product; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.product (id, sku, name, description, price, stock) FROM stdin;
1	416855601	DIP Switch	DIP Switch	54.94	52
3	436444719	Slide Switch	Slide Switch	8.81	62
4	170750924	Fuse	Fuse	44.99	36
5	246698236	Circuit Breaker	Circuit Breaker	2.42	8
6	065858667	Toggle Switch	Toggle Switch	32.74	43
7	829114529	Piezoelectric device	Piezoelectric device	82.89	8
8	296093492	Footswitch,	Footswitch,	64.72	48
9	320839069	Power Cord	Power Cord	63.24	41
10	016003144	Ultrasonic Motor	Ultrasonic Motor	62.4	99
11	601811409	Piezoelectric device	Piezoelectric device	11.52	14
12	426907966	Power Cord	Power Cord	72.73	31
13	195558973	Slide Switch	Slide Switch	85.55	65
14	242682415	Footswitch,	Footswitch,	45.04	62
15	221524727	Power Cord	Power Cord	26.28	18
16	307534682	Toggle Switch	Toggle Switch	38.88	10
17	312966895	DIP Switch	DIP Switch	63.79	67
18	520891795	Fuse	Fuse	19.87	61
19	711209040	Piezoelectric device	Piezoelectric device	70.03	93
20	912991824	Slide Switch	Slide Switch	44.29	9
2	988558635	Toggle Switch	Toggle Switch	11.55	23
\.


--
-- Name: customer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.customer_id_seq', 5, true);


--
-- Name: customer_order_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.customer_order_id_seq', 10, true);


--
-- Name: order_line_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.order_line_id_seq', 20, true);


--
-- Name: product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.product_id_seq', 20, true);


--
-- Name: customer pk_customer; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customer
    ADD CONSTRAINT pk_customer PRIMARY KEY (id);


--
-- Name: customer_order pk_order; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customer_order
    ADD CONSTRAINT pk_order PRIMARY KEY (id);


--
-- Name: order_line pk_orderline; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_line
    ADD CONSTRAINT pk_orderline PRIMARY KEY (id);


--
-- Name: product pk_product; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT pk_product PRIMARY KEY (id);


--
-- Name: customer_order fk_order_customer; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customer_order
    ADD CONSTRAINT fk_order_customer FOREIGN KEY (customer_id) REFERENCES public.customer(id);


--
-- Name: order_line fk_orderline_order; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_line
    ADD CONSTRAINT fk_orderline_order FOREIGN KEY (order_id) REFERENCES public.customer_order(id) ON DELETE CASCADE;


--
-- Name: order_line fk_orderline_product; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_line
    ADD CONSTRAINT fk_orderline_product FOREIGN KEY (product_id) REFERENCES public.product(id);


--
-- PostgreSQL database dump complete
--

